//
//  RestfulAPITest.swift
//  TakeMyOrder
//
//  Created by Julia on 04/08/2016.
//  Copyright © 2016 Julia. All rights reserved.
//

import XCTest
@testable import TakeMyOrder

class RestfulAPITest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAPILoading(){
        //Find some way to tell the test thread to wait
        let expectation:XCTestExpectation = self.expectationWithDescription("go online and load stuff")
        
        //Need a URL
        let url: NSURL = NSURL(string: "https://api.backendless.com/v1/data/Users")!
    
        //Need to load the URL
        let sessionConfig: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        sessionConfig.HTTPAdditionalHeaders = [
            "application-id" :  "128AD56E-A653-C338-FF4E-DD46DC1DBD00",
            "secret-key" :  "4C051FC5-21B3-B1E1-FF95-237CCDFA6900"
            
        ]
        
        let session: NSURLSession = NSURLSession(configuration: sessionConfig)
        let loadTask: NSURLSessionDataTask = session.dataTaskWithURL(url) { (responseData:NSData?, serverResponse:NSURLResponse?, otherError:NSError?) in
            //Need to check results
            if let httpResponse: NSHTTPURLResponse = serverResponse as? NSHTTPURLResponse{
                XCTAssert(httpResponse.statusCode == 200, "Status code should be 200,but we got \(httpResponse.statusCode)")
            }else{
                XCTAssert(false, "Couldn't get a HTTP response")
            }
            
            //convert data to string
            if let responseData = responseData{
                let responseString: String? = String(data: responseData,encoding: NSUTF8StringEncoding)
                
                print(responseString)
            }
            
            //Convert data to Json
            if let responseJson: [String : AnyObject] = try! NSJSONSerialization.JSONObjectWithData(responseData!, options: []) as? [String : AnyObject]{
                print("ResponseJson: \(responseJson)")
            }
            
            expectation.fulfill()
        }
        
        //Start the loading
        loadTask.resume()
        
        //Wait until expectations are met
        self.waitForExpectationsWithTimeout(30, handler: nil)
    }
    
    func testNilURL() {
        //let urlString: String = nil
        let brokenURL: NSURL? = NSURL(string: "has spaces")
        
        XCTAssert(brokenURL == nil, "We want broken URL")
    }
    
    func testLoadStringDirectly() {
        // Need a URL
        let url: NSURL = NSURL(string: "https://pokeapi.co")!
        
        print("Start loading: \(NSDate())")
        let responseString: String = try! String(contentsOfURL: url)
        print("Done loading: \(NSDate())")
    }
    
    func testBackendlessAPI() {
        // Find some way to tell the test thread to wait
        let expectation: XCTestExpectation = self.expectationWithDescription("go online and load stuff")
        
        // Need a URL
        let url: NSURL = NSURL(string: "https://api.backendless.com/v1/data/Restaurant")!
        
        // Need to load the URL
        let sessionConfig: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        sessionConfig.HTTPAdditionalHeaders = [
            "application-id" : "B64032DA-0154-435B-FF20-9CE052F7F400",
            "secret-key" :  "CD7062A1-06AD-B902-FFB2-D3C8E178D000"
        ]
        
        let session: NSURLSession = NSURLSession(configuration: sessionConfig)
        
        let loadTask: NSURLSessionDataTask = session.dataTaskWithURL(url) { (responseData: NSData?, serverResponse: NSURLResponse?, otherError: NSError?) in
            
            // Need to check results
            if let httpResponse: NSHTTPURLResponse = serverResponse as? NSHTTPURLResponse {
                XCTAssert(httpResponse.statusCode == 200, "Status code should be 200, but we got \(httpResponse.statusCode)")
            } else {
                XCTAssert(false, "Couldn't get a HTTP Response")
            }
            
        
            // Convert data to string
            if let responseData = responseData {
                let responseString: String? = String(data: responseData, encoding: NSUTF8StringEncoding)
                print(responseString)
            }
            
            // Convert data to JSON
            if let responseJson: [ String : AnyObject ] = try!
                NSJSONSerialization.JSONObjectWithData(responseData!, options: []) as? [String : AnyObject] {
                
                // Read the data Array from the returned JSON
                let dataArray: [[String: AnyObject]] = responseJson["data"] as! [[String : AnyObject]]
                
                var restaurants: [Restaurant] = []
                // Go thru each restaurant JSON
                for restaurantJson: [String : AnyObject] in dataArray {
                    // Find name
                    let name: String = restaurantJson["name"] as! String
                    let photo: String = restaurantJson["photo"] as! String
                    let type: String = restaurantJson["type"] as! String
                    let location: String = restaurantJson["location"] as! String
                    let restaurant: Restaurant = Restaurant(name: name, photo: nil, type: type, location: location)!
                    restaurants.append(restaurant)
                    print("Found JSON data for \(name)")
                }
                
                
            }
        
            expectation.fulfill()
        }
    
        // Start loading
        loadTask.resume()

        // Wait until expectation are met
        self.waitForExpectationsWithTimeout(30, handler: nil)
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}

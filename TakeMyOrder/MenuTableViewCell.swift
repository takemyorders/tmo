//
//  MenuTableViewCell.swift
//  TakeMyOrder
//
//  Created by Julia on 06/08/2016.
//  Copyright © 2016 Julia. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    var menu: Menu! {
        didSet {
            self.menuName.text = menu.name
            self.menuDescription.text = menu.descriptions
            self.menuPhoto.image = menu.photo
            if self.menuPrice != nil {
                self.menuPrice.text = "RM \(String(format: "%.2f", menu.price))"
            }
        }
    }
    
    @IBOutlet weak var menuName: UILabel!
    @IBOutlet weak var menuDescription: UILabel!
    @IBOutlet weak var menuPrice: UILabel!
    @IBOutlet weak var menuPhoto: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

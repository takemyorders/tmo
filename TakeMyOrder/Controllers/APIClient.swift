//
//  APIClient.swift
//  TakeMyOrder
//
//  Created by Jason Khong on 8/13/16.
//  Copyright © 2016 Julia. All rights reserved.
//

import Foundation

class APILoader {
    
    func saveRestaurantToServer(restaurant: Restaurant, completionBlock:( (success: Bool) -> Void )?) {
        // One day...
        
        // Save on the server
        
        // Run completionBlock to tell View Controller whether save was successful or not
    }

    func loadRestaurant(completionBlock:( (restaurants: [Restaurant]) -> Void )?) {
        
        // Find some way to tell the test thread to wait
        // let expectation: XCTestExpectation = self.expectationWithDescription("go online and load stuff")
        
        // Need a URL
        let url: NSURL = NSURL(string: "https://api.backendless.com/v1/data/Restaurant")!
        
        // Need to load the URL
        let sessionConfig: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        sessionConfig.HTTPAdditionalHeaders = [
            "application-id" : "B64032DA-0154-435B-FF20-9CE052F7F400",
            "secret-key" :  "CD7062A1-06AD-B902-FFB2-D3C8E178D000"
        ]
        
        let session: NSURLSession = NSURLSession(configuration: sessionConfig)
        
        let loadTask: NSURLSessionDataTask = session.dataTaskWithURL(url) { (responseData: NSData?, serverResponse: NSURLResponse?, otherError: NSError?) in
            
            // Convert data to string
            if let responseData = responseData {
                let responseString: String? = String(data: responseData, encoding: NSUTF8StringEncoding)
                print(responseString)
            }
            
            // Convert data to JSON
            var restaurants: [Restaurant] = []

            if let responseJson: [ String : AnyObject ] = try!
                NSJSONSerialization.JSONObjectWithData(responseData!, options: []) as? [String : AnyObject] {
                
                // Read the data Array from the returned JSON
                let dataArray: [[String: AnyObject]] = responseJson["data"] as! [[String : AnyObject]]
                
                // Go thru each restaurant JSON
                for restaurantJson: [String : AnyObject] in dataArray {
                    // Find name
                    let name: String = restaurantJson["name"] as! String
                    //                    let photo: UIImage = restaurantJson["photo"] as! UIImage
                    //                    let type: String = restaurantJson["type"] as! String
                    //                    let location: String = restaurantJson["location"] as! String
                    let restaurant: Restaurant = Restaurant(name: name, photo: nil, type: "", location: "")!
                    
                    restaurants.append(restaurant)
                }
                
            }

            dispatch_async(dispatch_get_main_queue(), {
                completionBlock?(restaurants: restaurants)
            })
        }

        loadTask.resume()
    }
    
}
//
//  MenuLoader.swift
//  TakeMyOrder
//
//  Created by Julia on 12/08/2016.
//  Copyright © 2016 Julia. All rights reserved.
//

import Foundation
import UIKit

class MenuLoader {
    //Manage data files
    let fileManager: NSFileManager = NSFileManager.defaultManager()
    
    //manage loading from server
    let session: NSURLSession
    let baseURL: NSURL
    
    static let sharedLoader: MenuLoader = MenuLoader()
    
    private init() {
        let config: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        config.HTTPAdditionalHeaders = [
            "application-id": "B64032DA-0154-435B-FF20-9CE052F7F400",
            "secret-key": "CD7062A1-06AD-B902-FFB2-D3C8E178D000",
            "application-type": "REST"
        ]
        
        self.session = NSURLSession(configuration: config)
        self.baseURL = NSURL(string: "https://api.backendless.com/v1/")!
    }
    
    func dataFileURL() -> NSURL {
        let docDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
        
        let filePath: NSURL = docDirectory.URLByAppendingPathComponent("data.plist")
        
        return filePath
    }
}




//
//  RestaurantTableViewCell.swift
//  TakeMyOrder
//
//  Created by admin on 23/07/2016.
//  Copyright © 2016 Julia. All rights reserved.
//

import UIKit

class RestaurantTableViewCell: UITableViewCell {
//MARK: Properties
    var restaurant: Restaurant! {
        didSet {
            self.nameLabel.text = restaurant.name
            self.logoImageView.image = restaurant.photo
        }
    }

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

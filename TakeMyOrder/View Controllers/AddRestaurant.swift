//
//  AddRestaurant.swift
//  TakeMyOrder
//
//  Created by admin on 30/07/2016.
//  Copyright © 2016 Julia. All rights reserved.
//

import UIKit

class AddRestaurant: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var addedRestaurant : Restaurant?
    var addedRestaurantBlock: ((addedRestaurant : Restaurant?)->Void)?
    
    
    @IBAction func selectImage(sender: UITapGestureRecognizer) {
        // Hide the keyboard
        restaurantTextField.resignFirstResponder()
        locationTextField.resignFirstResponder()
        restaurantTypeField.resignFirstResponder()
        // UIImagePickerController is a view controller that lets a user pick media from their photo library
        let imagePickerController = UIImagePickerController()
        // Only allow photos to be picked, not taken
        imagePickerController.sourceType = .PhotoLibrary
        // Make sure ViewController is notified when the user picks an image
        imagePickerController.delegate = self
        presentViewController(imagePickerController, animated: true, completion: nil)
        
    }
    
    @IBOutlet weak var addRestaurant: UILabel!
    @IBOutlet weak var restaurantNameLabel: UILabel!
    @IBOutlet weak var restaurantTextField: UITextField!
    @IBOutlet weak var restaurantLocationLabel: UILabel!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var restaurantTypeLabel: UILabel!
    @IBOutlet weak var restaurantTypeField: UITextField!
    @IBOutlet weak var restaurantPhotoLabel: UILabel!
    @IBOutlet weak var restaurantImageView: UIImageView!
    
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    
    @IBAction func saveRestaurant(sender: AnyObject) {
        self.addedRestaurant = Restaurant(name: self.restaurantTextField.text!, photo: self.restaurantImageView.image!, type: self.restaurantTypeField.text!, location: self.locationTextField.text!)
        self.addedRestaurantBlock?(addedRestaurant: self.addedRestaurant)
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func cancel(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        if saveButton === sender{
//            let name = restaurantTextField.text ?? ""
//            let location = locationTextField.text ?? ""
//            let type = restaurantTypeField.text ?? ""
//            let image = restaurantImageView.image
//            
//            addedRestaurant = Restaurant (name: name, photo: image, type: type, location: location)
//            
//        }
//        
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        // Dismiss the picker if the user canceled
        dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        // The info dictionary contains multiple representations of the image, and this uses the original
        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        // Set photoImageView to display the selected image
        restaurantImageView.image = selectedImage
        // Dismiss the picker
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

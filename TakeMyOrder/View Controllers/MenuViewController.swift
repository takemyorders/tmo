//
//  MenuViewController.swift
//  TakeMyOrder
//
//  Created by Julia on 16/07/2016.
//  Copyright © 2016 Julia. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    var restaurant: Restaurant!
    
    @IBOutlet weak var restaurantLogo: UIImageView!
    @IBOutlet weak var restautantName: UILabel!
    @IBOutlet weak var restaurantMenu: UILabel!
    
    @IBOutlet weak var menuListTableView: UITableView!
    
    var menus = [Menu]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("Menu for \(restaurant.name)")
        
        self.restautantName.text = self.restaurant.name
        self.restaurantLogo.image = self.restaurant.photo
        self.restaurantMenu.text = self.restaurant.type
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension MenuViewController : UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "MenuTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! MenuTableViewCell
        let menu = menus[indexPath.row]
        
        cell.menu = menu
        
        return cell
    }
}

extension MenuViewController : UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let menuVC: MenuViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MenuViewController") as! MenuViewController
        
        let menuRestaurant: Menu = self.menus[indexPath.row]
        
        self.navigationController?.pushViewController(menuVC, animated: true)
    }
}




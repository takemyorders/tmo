//
//  RestaurantViewController.swift
//  TakeMyOrder
//
//  Created by Julia on 16/07/2016.
//  Copyright © 2016 Julia. All rights reserved.
//

import UIKit

class RestaurantListViewController: UIViewController {
    
    @IBOutlet weak var SearchRestaurant: UISearchBar!    
    @IBOutlet weak var restaurantTableView: UITableView!
    
//    @IBAction func unwindtorestaurantlist(sender: UIStoryboardSegue){
//        if let sourceviewcontroller = sender.sourceViewController as? AddRestaurant,restaurant = sourceviewcontroller.addedRestaurant{
//            
//            //add a new restaurant
//            let newIndexPath = NSIndexPath(forRow: restaurants.count,inSection: 0)
//            restaurants.append(sourceviewcontroller)
//            tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Bottom)
//        }
//    }
    
    var restaurants = [Restaurant]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        loadRestaurant()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showAddRestaurant"{
            if let destVC: AddRestaurant = (segue.destinationViewController as? UINavigationController)?.viewControllers.first as? AddRestaurant {
                destVC.addedRestaurantBlock = {(addedRestaurant: Restaurant?) -> Void in
                    self.restaurants.append(addedRestaurant!)
                    self.restaurantTableView.reloadData()
                    
                }
            }
        }
    }
    
    func loadRestaurant() {
        let loader: APILoader = APILoader()
        loader.loadRestaurant { (restaurants: [Restaurant]) in
            self.restaurants = restaurants
            self.restaurantTableView.reloadData()
        }
    }
    
}
//        let restaurant1 = UIImage(named: "Burger King")
//        let detail1 = Restaurant(name: "Burger King", photo: restaurant1,type: "Fast Food", location: "Kuala Lumpur")
//        
//        let restaurant2 = UIImage(named: "KFC")
//        let detail2 = Restaurant(name: "KFC", photo: restaurant2,type: "Fast Food", location: "Selangor")
//        
//        let restaurant3 = UIImage(named: "McDonalds")
//        let detail3 = Restaurant(name: "McDonalds", photo: restaurant3,type: "Fast Food", location: "Perak")
//        
//        let restaurant4 = UIImage(named: "Subway")
//        let detail4 = Restaurant(name: "Subway", photo: restaurant4,type: "Fast Food", location: "Selangor")
//        
//        let restaurant5 = UIImage(named: "Starbucks")
//        let detail5 = Restaurant(name: "Starbucks", photo: restaurant5,type: "Fast Food", location: "Melaka")
//        
//        let restaurant6 = UIImage(named: "Chicken Rice Shop")
//        let detail6 = Restaurant(name: "Chicken Rice Shop", photo: restaurant6,type: "Fast Food", location: "Negeri Sembilan")
//
//        let restaurant7 = UIImage(named: "Boost")
//        let detail7 = Restaurant(name: "Boost", photo: restaurant7,type: "Fast Food", location: "Negeri Sembilan")
//
//        let restaurant8 = UIImage(named: "Secret Recipe")
//        let detail8 = Restaurant(name: "Secret Recipe", photo: restaurant8,type: "Fast Food", location: "Negeri Sembilan")
//
//        let restaurant9 = UIImage(named: "O'brien")
//        let detail9 = Restaurant(name: "O'brien", photo: restaurant9,type: "Fast Food", location: "Negeri Sembilan")
//
//        let restaurant10 = UIImage(named: "Pizza Hut")
//        let detail10 = Restaurant(name: "Pizza Hut", photo: restaurant10,type: "Fast Food", location: "Negeri Sembilan")
//
//        self.restaurants.append(detail1!)
//        self.restaurants.append(detail2!)
//        self.restaurants.append(detail3!)
//        self.restaurants.append(detail4!)
//        self.restaurants.append(detail5!)
//        self.restaurants.append(detail6!)
//        self.restaurants.append(detail7!)
//        self.restaurants.append(detail8!)
//        self.restaurants.append(detail9!)
//        self.restaurants.append(detail10!)

extension RestaurantListViewController : UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurants.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //let cell : UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Default , reuseIdentifier: "Cell")
        let cellIdentifier = "RestaurantTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! RestaurantTableViewCell
        let restaurant = restaurants[indexPath.row]
        
        cell.restaurant = restaurant;
        
        return cell
    }
}

extension RestaurantListViewController : UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let menuVC: MenuViewController =
            self.storyboard?.instantiateViewControllerWithIdentifier("MenuViewController") as! MenuViewController
        
        let menuRestaurant: Restaurant = self.restaurants[indexPath.row]
        menuVC.restaurant = menuRestaurant
        
        self.navigationController?.pushViewController(menuVC, animated: true)
    }
}

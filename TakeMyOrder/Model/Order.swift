//
//  Order.swift
//  TakeMyOrder
//
//  Created by Julia on 19/07/2016.
//  Copyright © 2016 Julia. All rights reserved.
//
// Declaration for Order

import Foundation
import UIKit

class Order {
    let quantity: Int = 0
    let totalPrice: Double = 0.0
}


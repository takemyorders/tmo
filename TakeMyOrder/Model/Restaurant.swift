//
//  Restaurant.swift
//  TakeMyOrder
//
//  Created by Julia on 19/07/2016.
//  Copyright © 2016 Julia. All rights reserved.
//
// Declaration for Restaurant

import Foundation
import UIKit

class Restaurant {
    var name: String
    var photo: UIImage?
    var type: String
    var location: String
    
    init?(name: String, photo: UIImage?, type: String, location: String) {
        self.name = name
        self.photo = photo
        self.type = type
        self.location = location
        
        if name.isEmpty {
            return nil
        }
    }
    
    
}


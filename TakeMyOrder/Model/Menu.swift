//
//  Menu.swift
//  TakeMyOrder
//
//  Created by Julia on 19/07/2016.
//  Copyright © 2016 Julia. All rights reserved.
//
// Declaration for Menu

import Foundation
import UIKit

class Menu {
    var name: String
    var photo: UIImage?
    var category: String
    var descriptions: String
    var price: Double = 0.0
    
    
    init?(name: String, category: String, descriptions: String, price: Double) {
        self.name = name
        self.category = category
        self.descriptions = descriptions
        self.price = price
        
        if name.isEmpty {
            return nil
        }
    }
}